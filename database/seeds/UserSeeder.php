<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email'=>'Admin@admin.com',
            'name'=>'admin',
            'password'=>bcrypt('admin'),                        
            'created_at'=>DB::raw('now()')           
        ]); 

        factory(User::class, 50)->create();

        DB::table('jobs')->delete();
    }
}
