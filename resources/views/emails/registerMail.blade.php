@component('mail::message')
# Registro exitoso

Su registro ha sido satisfactorio,
por favor establezca una contraseña para su usuario.

@component('mail::button', ['url' => url('setPassword/'.$codeChangePass)])
    Establecer contraseña
@endcomponent

Gracias<br>
@endcomponent
