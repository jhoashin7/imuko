<?php

namespace App\Services\User;

interface IUser {
    public function userList(); 
    public function userStore($data); 
    public function userUpdate($data,$user_id); 
    public function userDelete($user_id);
    public function setPassword($codeChangePass);
}