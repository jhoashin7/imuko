<?php

namespace App\Http\Controllers;

use App\Http\Requests\userStoreRequest;
use App\Http\Requests\userUpdateRequest;
use App\Services\User\IUser;
use App\User;

class UserController extends Controller
{
    private $_iUser;
    public function __construct(IUser $iUser)
    {
        $this->_iUser = $iUser;
    }
    public function index()
    {
        $users = $this->_iUser->userList();
        return view('users.user',['users'=>$users]);
    }


    public function store(userStoreRequest $request)
    {
        $user = $this->_iUser->userStore($request->all());
        return $user;
    }


    public function update(userUpdateRequest $request, User $user)
    {
        $user = $this->_iUser->userUpdate($request->all(),$user->id);
        return redirect("login");
    }


    public function destroy(User $user)
    {
        $response = $this->_iUser->userDelete($user->id);
        return $response;
    }

    
    public function setPassword($codeChangePass)
    {
        $response = $this->_iUser->setPassword($codeChangePass);
        return $response;
    }
}
