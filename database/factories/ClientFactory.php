<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'cod'       => $faker->numberBetween($min = 1000000000, $max = 9999999999),
        'name'      => $faker->firstName.' '.$faker->lastName,
        'city_id'   => rand ( 1 , 10),
    ];
});
