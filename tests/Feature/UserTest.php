<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use WithFaker;
    public function testUSerList()
    {
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $response = $this->get('users');

        $response->assertViewIs('users.user');        
    }
    public function testUserStore()
    {
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $name = $this->faker->firstName;
        $email = $this->faker->unique()->safeEmail;
        $response = $this->post('users',
        [
            'name'      => $name,
            'email'     =>$email
        ]);        
        $response->assertStatus(201);
    }

    public function testUserUpdate()
    {
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $user = User::all()->random(1);
        
        $response = $this->patch('users/'.$user[0]->id,
        [
            'password'          =>  Hash::make('jhonathan'),
        ]);        
        $response->assertStatus(302);
    }

    public function testUserDelete()
    {
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $user = User::all()->random(1);
        
        $response = $this->delete('users/'.$user[0]->id);
        $response->assertStatus(200);
    }

    public function testUserSetPass()
    {
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $user = User::whereNotNull('codeChangePass')->where('codeChangePass','!=',"")->get();
        
        $response = $this->get("setPassword/".$user[0]->codeChangePass);
        $response->assertStatus(200);
    }
}

