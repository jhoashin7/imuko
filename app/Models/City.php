<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 * 
 * @property int $id
 * @property int $departament_id
 * @property string $cod
 * @property string $city
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Departament $departament
 * @property Collection|Client[] $clients
 *
 * @package App\Models
 */
class City extends Model
{
	protected $table = 'cities';

	protected $casts = [
		'departament_id' => 'int'
	];

	protected $fillable = [
		'departament_id',
		'cod',
		'city'
	];

	public function departament()
	{
		return $this->belongsTo(Departament::class);
	}

	public function clients()
	{
		return $this->hasMany(Client::class);
	}
}
