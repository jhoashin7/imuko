 # Prueba desarrollo EVERTEC (PlaceTPay)
 
 _Prueba de aspiración a desarrollardor laravel_
 
 ## Comenzando 🚀
 
 _Ruta para clonar el repositorio._
 
 ```
 git clone https://jhoashin7@bitbucket.org/jhoashin7/placetopay.git
 ```
 
 
 ### Pre-requisitos 📋
 
-_PHP >= 7.1.3 y demas requerimientos para la funcionalidad de laravel ._
-_Debes activar el la extenciòn SOAP, esto se hace en el php.ini, esto con el fin de usar la librerisa "dnetix/redirection"
-si deseas mas informacion la puedes consultar en https://github.com/dnetix/redirection/blob/master/README.md ._
+_PHP >= 7.1.3 y demás requerimientos para la funcionalidad de laravel ._
+_Debes activar el la extenciòn SOAP, esto se hace en el php.ini, esto con el fin de usar la librería "dnetix/redirection"
+si deseas mas información la puedes consultar en https://github.com/dnetix/redirection/blob/master/README.md ._
 
 
 ### Instalación 🔧
 
 _Sebes de instalar las dependencias  con composer_
 
 ```
 composer install
 ```

## Comandos ⌨️

 ### Crear base de datos por comando.

 _Comando para crear base de datos, para esto la variable **DB_DATABASE** en el archivo **.env** no debe tener valor
 este comando creara la base de datos y actualizara el campo automaticamente_
 ```
 php artisan create:database imuko mysql utf8mb4-unicode
 ```
### Migrar e insertar datos iniciales

 _El siguiete comando ejecuta las migraciones y los seeders, en el caso de la tabla users, este tiene un usuario por defecto con las siguientes credenciales **email : Admin@admin.com** - **password : admin** esto con el fin de tener un usuario inicial que pueda ingresar y fertionar los datos_
 ```
 php artisan run:migrateSeed
 ```
 
 ## Ejecutando las pruebas ⚙️
 
 _laravel usa PHPUNIT para realizar pruebas unitarias, puedes hacerlas directamente en la terminal corriendo el comando _
 ```
 vendor/bin/phpunit
 ```

 ## Construido con 🛠️    
 
 * [Laracel 5.8](https://laravel.com/) - El framework web usado
 * [Composer](https://getcomposer.org/) - Manejador de dependencias
 
 
 
 
 ## Autores ✒️
 
 * **JHONATHAN ASCENCIO** - Jhonathan.ascenciog@gmail.com
