<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Client
 * 
 * @property int $id
 * @property int $user_id
 * @property int $city_id
 * @property string $cod
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property City $city
 * @property User $user
 *
 * @package App\Models
 */
class Client extends Model
{
	protected $table = 'clients';

	protected $casts = [
		'user_id' => 'int',
		'city_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'city_id',
		'cod',
		'name'
	];

	public static function clientJoinCities($city_id)
	{
		$where = "";
		if($city_id != "all")
			$where = "and city_id =".$city_id;
		return Client::join('cities','cities.id','=','clients.city_id')
					 ->select(DB::raw('clients.name,clients.cod,clients.id,cities.city,clients.city_id'))
					 ->whereRaw('cities.city is not null '.$where)
					 ->paginate(10);
	}
	public function city()
	{
		return $this->belongsTo(City::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
