<?php

namespace App\Services\Client;

interface IClient {
    public function clientList($city_id); 
    public function clientStore($data); 
    public function clientUpdate($data,$user_id); 
    public function clientDelete($user_id);
}