<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Departament
 * 
 * @property int $id
 * @property int $cod
 * @property string $departament
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|City[] $cities
 *
 * @package App\Models
 */
class Departament extends Model
{
	protected $table = 'departaments';

	protected $casts = [
		'cod' => 'int'
	];

	protected $fillable = [
		'cod',
		'departament'
	];

	public function cities()
	{
		return $this->hasMany(City::class);
	}
}
