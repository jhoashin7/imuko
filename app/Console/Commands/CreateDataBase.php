<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use PDO;

class CreateDataBase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:database {nameDB} {type?} {collation?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se crea la base de datos con sus migraciones ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function getCotejamiento($cotejamiento)
    {
        switch ($cotejamiento) {
            case 'utf8':
                $cot = "CHARACTER SET utf8 COLLATE utf8_general_ci";
                break;
            case 'utf8-unicode':
                $cot = "CHARACTER SET utf8 COLLATE utf8_unicode_ci";
                break;
            case 'utf8mb4':
                $cot = "CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci";
                break;
            case 'utf8mb4-unicode':
                $cot = "CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci";
                break;
            
            default:
                $cot = "CHARACTER SET utf8 COLLATE utf8_general_ci";
                break;
        }
        return $cot;
    }

    public function changeEnvDB_DATABASE($nombrebd)
    {
        $path = base_path('.env');

            if (file_exists($path)) {
                file_put_contents($path, str_replace(
                    'DB_DATABASE='.env('DB_DATABASE'),'DB_DATABASE='.$nombrebd,file_get_contents($path)
                ));
            }
    }
    
    public function createDataBase($nameDB,$type,$collation)
    {
        $crearbd = DB::connection($type)->select("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = "."'".$nameDB."'");
 
        if(empty($crearbd)) {
            DB::connection($type)->select('CREATE DATABASE '. $nameDB .' '.$collation);
            $this->info("La Base de Datos '$nameDB' de type '$type' con collation '$collation' ha sido creada Correctamente ! ");
        }
        else {
            $this->info("La Base de Datos con el nombre '$nameDB' ya existe ! ");
        }    
    }
    public function handle()
    {
        try {
            $nameDB = $this->argument('nameDB');
            $type = $this->hasArgument('type') && $this->argument('type') ? $this->argument('type'): DB::connection()->getPDO->getAttribute(PDO::ATTR_DRIVER_NAME);
            $collation = $this->argument('collation');

            $this->changeEnvDB_DATABASE($nameDB);
            $cot = $this->getCotejamiento($collation);
            $this->createDataBase($nameDB,$type,$cot);
        }
 
        catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    
}
