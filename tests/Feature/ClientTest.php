<?php

namespace Tests\Feature;

use App\Models\City;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\UserFake;

class ClientTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use WithFaker;
    
    public function testClienList()
    {
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $response = $this->get('clients/city/all');

        $response->assertViewIs('clients.client');
    }

    public function testClientStore()
    {
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $city = City::all()->random(1)->first();
        $cod = $this->faker->numberBetween($min = 1000000000, $max = 9999999999);
        $name = $this->faker->firstName.' '.$this->faker->lastName;
        $response = $this->post('clients',
        [
            'city_id'   => $city->id,
            'cod'       => $cod,
            'name'      => $name
        ]);        
        $response->assertStatus(201);
    }

    public function testClientUpdate()
    {
        
        $city = City::all()->random(1);
        $user = User::all()->random(1);
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $response = $this->patch('clients/'.$user[0]->id,
        [
            'city_id'   => $city[0]->id,
            'cod'       => $this->faker->numberBetween($min = 1000000000, $max = 9999999999),
            'name'      => $this->faker->firstName.' '.$this->faker->lastName
        ]);        
        $response->assertStatus(200);
    }
    public function testClientDelete()
    {
        $user = User::all()->random(1);
        $this->userFake  = new User([
            'id' => 1,
            'name' => 'yish'
        ]);
        $this->be($this->userFake);
        $response = $this->patch('clients/'.$user[0]->id);
        $response->assertStatus(200);
    }
}
