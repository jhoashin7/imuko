<?php

namespace App;

use App\Models\Client;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
		'password'
	];

	protected $fillable = [
		'name',
		'email',
		'password',
		'codeChangePass'
	];

	public function clients()
	{
		return $this->hasMany(Client::class);
	}
}
