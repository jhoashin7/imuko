<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          @csrf
          <input type="text" id="id" hidden>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Codigo:</label>
            <input type="text" class="form-control" id="cod" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre:</label>
            <input type="text" class="form-control" id="name" required>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Ciudad:</label>
            <Select id="city_id" class="form-control" required>
                @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->city}}</option>
                @endforeach
            </Select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveClient()" id="update">Actualizar</button>
        <button type="button" class="btn btn-primary" onclick="saveClient()" id="save">Crear</button>
      </div>
    </div>
  </div>
</div>
<script>
    function dataclient(id,cod,name,city_id){
        $("#id").val(id);
        $("#cod").val(cod);
        $("#name").val(name);
        $("#city_id").val(city_id);
        $("#city_id").change();

        if(id == "")
        { 
            $('#update').hide();
            $('#save').show();
        }else{
            $('#update').show();
            $('#save').hide();
        }
    }

    function saveClient(){
        var id      =   $("#id").val();
        var cod     =   $("#cod").val();
        var name    =   $("#name").val();
        var city_id =   $("#city_id").val();
        
        if(id == "")
        { 
          var url =  "{{url('clients')}}";
          var method =  "post";
          var message = "Cliente Creado."
        }else{
          var url =  "{{url('clients')}}/"+id;
          var method =  "patch";
          var message = "Cliente actualizado."
        }

        $.ajax({
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            url:    url ,
            type:   method,
            data:   { 
                cod     :cod,
                name    :name,
                city_id :city_id
            },
            success : function(response, textStatus, jqXhr) {
              alert(message);
            },
            error : function(jqXHR, textStatus, errorThrown) {
              var errors = jqXHR.responseJSON.errors;
              var messErr;
              $.each(errors,function(index, value){
                messErr += "\n"+value ;
              })
              alert(messErr);
            },
        });
      }

      function deleteclient(id){  
        var r = confirm("Esta seguro de eliminar el usuario");
        if (r == true) {
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:     "{{url('clients')}}/"+id,
            type:   "delete",
            success : function(response, textStatus, jqXhr) {
              alert('Cliente eliminado ');
            },
          });
        } else {
          close();
        }
      }
</script>