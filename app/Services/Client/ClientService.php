<?php 
namespace App\Services\Client;

use App\Models\City;
use App\Models\Client;
use App\User;

class ClientService implements IClient{
    public function clientList($city)
    {
        $data["clients"] = Client::clientJoinCities($city);
        $data["cities"]  = City::all();     

        return $data;
    }
    public function clientStore($data)
    {
        return Client::create($data);
    }
    public function clientUpdate($data, $client_id)
    {
        return Client::where('id',$client_id)
                     ->update($data);
    }

    public function clientDelete($client_id)
    {
        return Client::where('id',$client_id)
                     ->delete();
    }
}