<?php 
namespace App\Services\User;

use App\User;

class UserService implements IUser{
    public function userList()
    {
        $users = User::paginate(10);
        return $users;
    }

    public function userStore($data)
    {
        $data['codeChangePass'] =  substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 100);
        $userCreated            =  User::create($data);

        return $userCreated;
    }

    public function userUpdate($data, $user_id)
    {
        User::where('id',$user_id)
        ->update([
            "password"         => bcrypt($data["password"]),
            'codeChangePass'   => ""
        ]);
    }

    public function userDelete($user_id)
    {
        return User::where('id',$user_id)->delete();
    }

    public function setPassword($codeChangePass)
    {
        $user = User::where('codeChangePass',$codeChangePass)->first();
        if($user)
            return view("users.password",['user_id' => $user->id]);
        else
            abort(404);
    }
}