<?php

namespace App\Providers;

use App\Observers\UserObserver;
use App\Services\Client\ClientService;
use App\Services\Client\IClient;
use App\Services\User\IUser;
use App\Services\User\UserService;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IUser::class,
                        UserService::class);
                        
        $this->app->bind(IClient::class,
                        ClientService::class);
        if ($this->app->environment() == 'local') {
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
    }
}
