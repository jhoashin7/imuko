<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          @csrf
          <input type="text" id="id" hidden>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre:</label>
            <input type="text" class="form-control" id="name">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Email:</label>
            <input type="text" class="form-control" id="email">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveUser()" id="update">Actualizar</button>
        <button type="button" class="btn btn-primary" onclick="saveUser()" id="save">Crear</button>
      </div>
    </div>
  </div>
</div>
<script>
    function dataUser(id,name,email){
        $("#id").val(id);
        $("#name").val(name);
        $("#email").val(email);

        if(id == "")
        { 
            $('#update').hide();
            $('#save').show();
            $('#email').prop( "disabled", false );
        }else{
            $('#update').show();
            $('#save').hide();
            $('#email').prop( "disabled", true );
        }
    }

    function saveUser(){
        var id      =   $("#id").val();
        var name    =   $("#name").val();
        var email   =   $("#email").val();
        
        if(id == "")
        { 
          var url =  "{{url('users')}}";
          var method =  "post";
          var message = "Usuario Creado."
        }else{
          var url =  "{{url('users')}}/"+id;
          var method =  "patch";
          var message = "Usuario actualizado."
        }

        $.ajax({
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            url:    url ,
            type:   method,
            data:   { 
                name    :name,
                email   :email
            },
            success : function(response, textStatus, jqXhr) {
              alert(message);
            },
            error : function(jqXHR, textStatus, errorThrown) {
              var errors = jqXHR.responseJSON.errors;
              var messErr;
              $.each(errors,function(index, value){
                messErr += "\n"+value ;
              })
              alert(messErr);
            },
        });
      }

      function deleteUser(id){  
        var r = confirm("Esta seguro de eliminar el usuario");
        if (r == true) {
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:    "{{url('users')}}/"+id,
            type:   "delete",
            success : function(response, textStatus, jqXhr) {
              alert('Usuario eliminado ');
            },
          });
        } else {
          close();
        }
      }
</script>