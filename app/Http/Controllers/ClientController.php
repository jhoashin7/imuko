<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientStoreRequest;
use App\Models\City;
use App\Models\Client;
use App\Services\Client\IClient;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    private $_iClient;
    public function __construct(IClient $iClient)
    {
        $this->_iClient  = $iClient;
    }
    
    public function ListCLients($city)
    {
        $response = $this->_iClient->clientList($city);
        return view('clients/client',["clients" => $response['clients'],'cities'=>$response["cities"],"currentCity"=>$city]);
    }


    public function store(ClientStoreRequest $request)
    {
        $clientCreated = $this->_iClient->clientStore($request->all());
        return $clientCreated;
    }

  
    public function update(Request $request, Client $client)
    {
        $clientUpdated =  $this->_iClient->clientUpdate($request->all(),$client->id);
        return $clientUpdated;
    }


    public function destroy(Client $client)
    {
        $response = $this->_iClient->clientDelete($client->id);
        return $response;
    }

}
